import numpy as np

# Define the pre-shock and post-shock states
rho1 = 1.0
u1 = 0.0
p1 = 1.0

rho2 = 2.0
u2 = 0.5
p2 = 2.0

# Calculate the shock speed
a1 = np.sqrt(p1 / rho1)
a2 = np.sqrt(p2 / rho2)
shock_speed = (u2 - u1) / (1 - rho1 / rho2)

# Calculate the post-shock velocity and pressure
u2_post = (u1 + shock_speed * (1 - rho1 / rho2)) / (1 - rho2 / rho1)
p2_post = p1 + rho1 * (u1 - u2_post) * (u1 + a1) + rho2 * (u2_post - u2) * (u2 + a2)

# Print the results
print("Shock speed: {:.2f}".format(shock_speed))
print("Post-shock velocity: {:.2f}".format(u2_post))
print("Post-shock pressure: {:.2f}".format(p2_post))